#!/usr/bin/env python2.7
## listen.py for  in /home/eax/dev/badbios
## 
## Made by kevin soules
## Login   <soules_k@epitech.net>
## 
## Started on  Thu Nov 28 22:45:41 2013 kevin soules
## Last update Mon Dec  2 00:13:21 2013 kevin soules
##

import time
import pyaudio
import signal
import threading
import itertools
from numpy import fromstring, fft, int32
from scipy import fft

NUM_SAMPLES = 4096
SAMPLING_RATE = 192000

AUDIBLE = False
# AUDIBLE = True

FREQ_START = 17100
FREQ_INCR = 127
TIME_OF_WAVE = 0.08 # (in second)
MIN_PRES = 0.7 # Minimum percentage of the frequence in the last TIME_OF_WAVE seconds to be accepted
if AUDIBLE:
    FREQ_START /= 2


p = pyaudio.PyAudio()
chunks=[]
stop = False

def signal_handler(signal, frame):
    global stop
    stop = True

def stream():
    global chunks, inStream
    while not stop:
        audio_data = fromstring(inStream.read(NUM_SAMPLES), dtype=int32)
        normalized_data = audio_data / 2147483648.0
        chunks.append(normalized_data)

def record():
    global inStream
    inStream = p.open(format=pyaudio.paInt32,channels=1,\
                      rate=SAMPLING_RATE,input=True,frames_per_buffer=NUM_SAMPLES)
    stream()

def isFreqSpotted(data, toSpot):
    lenData = float(len(data))
    maxFreq = float(SAMPLING_RATE/2)

    # print lenData, maxFreq
    lowspot = int((toSpot - 400) / maxFreq * lenData)
    highspot = int((toSpot + 400) / maxFreq * lenData)

    tmp = [v for v in enumerate(data[lowspot:highspot+1])]
    freqtupple = sorted(tmp, key=lambda k: k[1])
    toSpotIndex = int(toSpot / maxFreq * lenData)
    if abs(freqtupple[-1][0] + lowspot - toSpotIndex) <= 1:
        spotFreq = freqtupple[-1][1]
    elif abs(freqtupple[-2][0] + lowspot - toSpotIndex) <= 1:
        spotFreq = freqtupple[-2][1]
    else:
        return False

    # Try to avoid false detection
    aroundFreq = [v[1] for v in freqtupple[2:-5]]
    aroundFreq = sum(aroundFreq) / float(len(aroundFreq))

    realSpotFreq = ((lowspot + freqtupple[-1][0]) / lenData * maxFreq)

    if spotFreq > 0.04 and spotFreq/aroundFreq > 3.5:
        return True
    return False

def listen(data, freqsToSpot):
    data =  abs(fft(data))[:NUM_SAMPLES/2]
    ret = []
    for f in freqsToSpot:
        ret.append(isFreqSpotted(data, f))

    return ret


def launch():
    threading.Thread(target=record).start()

    activated = False
    lastSecond = []
    hexstr = ""
    lastPrinted = -1
    cntBits = 0
    receivedMsg = ""
    correspondances = [i for i in itertools.combinations(range(24), 2)][:255]

    while not stop:
        while not len(chunks):
            time.sleep(0.0005)

        data = chunks.pop(0)

        ret = listen(data, [FREQ_START + i * FREQ_INCR for i in xrange(24)])
        ret = map(int, ret)
        now = time.time()
        lastSecond.append([now, ret])
        if lastSecond[-1][0] - lastSecond[0][0] >= TIME_OF_WAVE:
            freqs = [[val[1][i] for val in lastSecond] for i in range(24)]
            presences = map(lambda x: sum(x) / float(len(x)), freqs)

            maxPres1 = max(presences)
            spotted1 = presences.index(maxPres1)
            presences[spotted1] = 0
            maxPres2 = max(presences)
            spotted2 = presences.index(maxPres2)
            if maxPres1 > MIN_PRES and maxPres2 > MIN_PRES:
                lastSecond = [[now, [0]*24]]
                if (spotted1, spotted2) in correspondances:
                    hexstr += "%02x" % correspondances.index((spotted1, spotted2))
                elif (spotted2, spotted1) in correspondances:
                    hexstr += "%02x" % correspondances.index((spotted2, spotted1))
                else:
                    print "Woot, received unknown byte"
                    hexstr += hex(ord("?"))[2:]

                cntBits += 1

            while lastSecond[-1][0] - lastSecond[0][0] >= TIME_OF_WAVE:
                lastSecond.pop(0)

        if len(hexstr) == 2 and lastPrinted != cntBits:
            res = hexstr.decode("hex")
            hexstr = ""
            if not activated and res == "\2":
                activated = True
                print "Start of text !"
                stx = time.time()
            elif activated and res == "\3":
                activated = False
                print "End of text !"
                etx = time.time()
                elapsed = (etx - stx)
                print "The transmission took %f seconds. (%.2fbps)" % (elapsed, len(receivedMsg) * 8 / float(elapsed))
                receivedMsg = ""
            else:
                receivedMsg += res
                print "Received string [%s]" % (receivedMsg)
            lastPrinted = cntBits

        if len(chunks) > 20:
            print "Warning, script too slow. %d chunks to read." % len(chunks)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    launch()
