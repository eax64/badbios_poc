#!/usr/bin/env python2.7
## sender.py for  in /home/eax/dev/badbios
## 
## Made by kevin soules
## Login   <soules_k@epitech.net>
## 
## Started on  Thu Nov 28 23:27:27 2013 kevin soules
## Last update Mon Dec  2 00:15:25 2013 kevin soules
##

import time
import math
import array
import pyaudio
import binascii
from itertools import *

AUDIBLE = False
# AUDIBLE = True

FREQ_START = 17100
FREQ_INCR = 127
TIME_OF_WAVE = 1 # (in deci-second, must be an integer)
TIME_PAUSE_BETWEEN_WAVES = 0.06

if AUDIBLE:
    FREQ_START /= 2

def sine_wave(frequency=440.0, framerate=88200, amplitude=0.5):
    period = int(framerate / frequency)
    if amplitude > 1.0: amplitude = 1.0
    if amplitude < 0.0: amplitude = 0.0
    lookup_table = [float(amplitude) * math.sin(2.0*math.pi*float(frequency)*(float(i)/float(framerate))) for i in xrange(framerate / 10)]
    return lookup_table

p = pyaudio.PyAudio()
stream = p.open(rate=88200, channels=2, format=pyaudio.paFloat32, output=True)

correspondances = [i for i in combinations(range(24), 2)][:255]

waves = map(lambda x: sine_wave(x) * TIME_OF_WAVE, [FREQ_START + i * FREQ_INCR for i in xrange(24)])

#msg = "hello, world"
msg = "Hey ! C'est cool, ca marche presque. Y'a juste trop de corruptions, mais bon ca va on arrive a lire quand meme ! :D"
msg = "\2" + msg + "\3"

print "Pregenerate waves."
dblWaves = []
waveLen = len(waves[0])

# Speed up pregenerating. Only used char are generated.
ordMsg = map(ord, msg)
ordMsg = [x in ordMsg for x in xrange(257)]

cnt = 0
for c in correspondances:
    newWave = []

    if ordMsg[cnt]:
        for i in xrange(waveLen):
            newWave.append(waves[c[0]][i])
            newWave.append(waves[c[1]][i])

        dblWaves.append(array.array('f', newWave).tostring())
    else:
        dblWaves.append(0)
    cnt += 1

print "Pregeneration terminated"
print "Start sending the message : [%s]" % msg

for c in msg:
    stream.write(dblWaves[ord(c)])
    time.sleep(TIME_PAUSE_BETWEEN_WAVES)
stream.close()
p.terminate()
